# **Manual по Gitlab**
## **Cоздание личного репозитория с нужным .gitignore и простым README.MD**
1. Перейди в рабочую директорию на локальной машине ```cd Users/<...> #вместо <...> введи свой login```
2. Инициализируй репозиторий ```git init```

![](https://cdn.javarush.ru/images/article/5dbf6234-0ea6-40a8-9d37-ce14609d53b4/800.webp)

3. Введи имя и e-mail 

	```
	$ git config --global user.name "Your Name" 
	$ git config --global user.email you@example.com
	```
	Инициализировать git-репозиторий придется только один раз за проект (и тебе больше не придется вводить имя пользователя и адрес электронной почты).
5. Git будет отслеживать изменения всех файлов и каталогов в заданной директории, однако некоторые из них мы предпочли бы игнорировать. Для этого нам нужно создать файл **.gitignore** в корневом каталоге репозитория. Открой редактор и создай новый файл, например с таким содержанием:

	```
    # Kernel Module Compile Results
	*.mod*
    *.cmd
    .tmp_versions/
    modules.order
    Module.symvers
    Mkfile.old
    dkms.conf
    ```
	Помни, что каждый .gitignore индивидуален.
6. Создай файл **README.md** в корневом каталоге с простым описанием проекта.
## **Cоздание веток develop и master**
Репозиторий создан! Изначально мы находимся на ветке **master**.
Чтобы создать дополнительную ветку develop пишем такой код: 

```git checkout -b develop``` 

При этом мы уже перешли на новую ветку develop.
## **Установка ветки develop по умолчанию**
Чтобы установить ветку develop по умолчанию выполни следующую команду 

```
git config --global init.defaultBranch {branchName}
```

![](https://cdn.javarush.ru/images/article/19d396fc-af1c-4d3f-8deb-2dbba09dfaf2/800.webp)

## **Создание issue на создание текущего мануала**
Давайте создадим новую задачу (*issue*) на создание текущего мануала. 
* Перейдите на панель управления вашего проекта ***> Проблемы > Новая проблема***
* В открытой задаче в вашем проекте нажмите кнопку с вертикальным многоточием ( ), чтобы открыть раскрывающееся меню, а затем нажмите **«Новая проблема»**, чтобы создать новую задачу в том же проекте
* На панели инструментов вашего проекта щелкните значок плюса ( + ), чтобы открыть раскрывающееся меню с несколькими параметрами. ***Выберите New Issue***, чтобы создать проблему в этом проекте
* На доске задач создайте новую задачу, щелкнув **знак плюса ( + )** вверху списка. Он открывает новую проблему для этого проекта, предварительно помеченную соответствующим списком.
* При создании новой задачи заполните следующие поля: 
	* Название
	* Описание задачи
	* Срок сдачи задачи 
## **Создание ветки по issue**
Если вы создадите ветвь с именем <issue-number>-issue-description и переместите ее в gitlab, она автоматически будет связана с этой проблемой. 
>Например, если у вас есть проблема с идентификатором 654 , и вы создадите ветвь с именем 654-some-feature и переместите ее в gitlab, она будет связана с проблемой 654.
Gitlab даже спросит вас, хотите ли вы создать запрос на слияние, и автоматически добавит Closes #654 в описание запроса на слияние, которое закроет issue 654 , когда запрос на слияние будет принят.
Кроме того, если вы перейдете на страницу данной проблемы на gitlab, вы увидите кнопку New Branch , которая автоматически создаст ветвь с именем формы <issue-number>-issue-description.

![](https://gitlab.com/gitlab-org/gitlab-foss/uploads/3280301a4e0378a917e1a3705058b310/screenshot.PNG)

## **Создание merge request по ветке в develop**
После того, как был сделан git push через консоль, в ней отобразится *ссылка*, перейдя по которой сразу откроется страница создания Merge Request-а.

![](https://gblobscdn.gitbook.com/assets%2F-Lc9m5pdTeXvEkKkPMQr%2F-MRPWH8e7z7RyemHFA-M%2F-MRPX0QmJ1xYMwygHXDG%2Fimage.png?alt=media&token=6b12f468-76f9-4f0b-a671-f804f9b77e4d)

В этом случае у Merge Request будет автоматически установлены исходная (ваша) ветка, и целевая ветка (куда будут вливаться изменения). По умолчанию, целевой веткой проекта является ***master***. Изменить целевую ветку на другую можно будет кликнув по ссылке **"Change branches"**.
## **Комментирование и принятие реквеста**
Допустим, вы внесли какие-то изменения. Эти изменения вас устраивают, и вы хотите создать запрос на принятие изменений (**Pull request**). 

![](https://git-scm.com/book/en/v2/images/blink-06-final.png)

В Pull request ваши коллеги смогут проверить внесенные изменения и обсудить их. Pull request можно создавать по любому поводу, будь то внесение конечных изменений или просьба о помощи в решении какой-либо проблемы.

Эмммм… это делается через сайт?

> Да, все это делается с сайта GitHub.

Pull request создается по нажатию одноименной кнопки, о которой мы говорили ранее при редактировании README-файла. 

***Элементарно!***

А еще вы можете создать отдельную ветку на сайте через сам репозиторий. Перейдите в репозиторий и кликните по выпадающему меню в левой части экрана. Там еще написано Branch: master. 

Задайте имя новой ветки и выберите *Create branch* (либо нажмите Enter на клавиатуре). Теперь у вас есть две одинаковые ветки. Это отличное место для внесения изменений и тестирования их до слияния с master.
Если это ваш репозиторий, то слить изменения с master можно через зеленую кнопку Merge pull request. Нажмите Confirm merge. Сразу после объединения нужной ветки с master нажмите Delete branch в фиолетовом боксе.

> Если вы участвуете в чужом проекте, то у участников команды (или проверяющего коммиты) могут возникнуть вопросы или замечания. Хотите внести какие-то изменения? Сейчас — самое время. Сразу по завершению изменений участники проекта смогут развертывать эти изменения напрямую из ветки и проводить конечное тестирование до слияния с master. Вы также сможете произвести развертку изменений для проверки их в рабочей среде.

После утверждения изменений необходимо произвести слияние вашего кода с веткой master. В Pull request хранится запись о ваших изменениях. Таким образом, вы всегда сможете открыть этот запрос и понять, какие изменения были сделаны и почему.
## **Формирование стабильной версии в master с простановкой тега**
Теги в Git есть двух основных типов:
* Теги с аннотациями, содержат сообщение, и имя и почту автора тега, как при коммите
* Легковесные теги — просто указатель на определённый коммит, без всякой допонительной информации
Для добавления тега с аннотацией — используем -a или --annotate:

```git tag -a v1.0 -m "Init version"```
Что бы получить список всех тегов — просто вызываем tag без аргументов:

```
git tag
v1.0
v1.0lw
v2.0
```
В случае легковесного тега сохраняется информация только о самом коммите, но нет ничего об авторе тега.
push тегов в репозиторий.

Чтобы сохранить теги в удалённый репозиторий — их необходимо пушить отдельно от коммитов.
Клоним тестовый репозиторий с Гитлаба:

```git clone git@gitlab.com:setevoy2/tests.git```
## **Работа с wiki проекта**
**itlab wiki** — документация, которую можно вести для каждого проекта. Обычно это системные требования, описание пакетов и процесса установки.

**gitlab wiki** — документация проектов
После добавления проекта в меню слева доступен раздел Wiki. В нем модно добавить неограниченное количество страниц с текстовыми материалами. Wiki специфична для проекта.

Первой всегда добавляется *главная страница Home*.

![](https://server-gu.ru/wp-content/uploads/2019/02/wiki2.png)

Стандартным способом оформления является Markdown. Доступен режим Preview в котором видно как будет выглядеть текст.

>В примере добавление заголовка h1 и выделение для команды python3 index.py

Чуть ниже показано как создать ссылку, ее текст указывается в скобках, сама ссылка — в квадратных скобках. Пока оставляем их пустыми и нажимаем New Page. Позже в квадратных скобках будет адрес второй страницы.

Title можно оставить «how to setup». Далее содержимое в том же формате. Здесь это список.

![](https://server-gu.ru/wp-content/uploads/2019/02/wiki3.png)